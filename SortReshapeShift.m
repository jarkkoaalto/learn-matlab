A = [78 23 10 100 45 5 6];
disp(sort(A));

C = [1 2 3 4
    5 6 7 8
    3 5 6 6];

B = reshape(C,2,6);
disp(B);

D = [1 2 3; 4 5 6; 7 8 9];
R = circshift(D,1);
disp(R);
