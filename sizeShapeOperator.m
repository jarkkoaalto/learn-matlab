b = [1 2 3 4];
disp(length(b));

A = [ 1 2; 3 4];
disp(size(A));

A = [1 2; 3 4];
disp(reshape(A, 4, 1));

A=[5 6; 7 8];
disp(ndims(A));

B=[5 6; 7 8; 4 5];
disp(ndims(B));